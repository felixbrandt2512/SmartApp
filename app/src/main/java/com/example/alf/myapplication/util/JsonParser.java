package com.example.alf.myapplication.util;

import com.example.alf.myapplication.MessagePOJO.Player;
import com.example.alf.myapplication.MessagePOJO.Subscription;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class JsonParser {

    public static Subscription parseMessage(String message){
        try {
            JSONObject obj = new JSONObject(message);
            Subscription sub = new Subscription(
                    obj.getString("machine"),
                    genList(obj.getJSONArray("topics")),
                    genList(obj.getJSONArray("events"))
            );
            return sub;
        } catch (JSONException e) {
            e.printStackTrace();
            return new Subscription("",new ArrayList<String>(),new ArrayList<String>());
        }
    }

    public static String getPlayerMachine(String message){
        try {
            JSONObject obj = new JSONObject(message);
            return obj.getString("machine");
        } catch (JSONException e) {
            e.printStackTrace();
           return "";
        }
    }

    public static Player parsePlayer(String message){
        try {
            JSONObject obj = new JSONObject(message);
            Player sub = new Player(
                    obj.getString("name")
            );
            return sub;
        } catch (JSONException e) {
            e.printStackTrace();
            return new Player("");
        }
    }

    private static List<String> genList(JSONArray jsonArray) throws JSONException {
        List<String> list = new ArrayList<String>();
        for (int i=0; i<jsonArray.length(); i++) {
            list.add( jsonArray.getString(i) );
        }
        return list;
    }
}
