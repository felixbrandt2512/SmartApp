package com.example.alf.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.example.alf.myapplication.adapter.MachineViewAdapter;
import com.example.alf.myapplication.util.JsonParser;

public class MaschineView extends AppCompatActivity {

    TextView textView;
    RecyclerView _recy;
    String newString;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maschine_view);

        _recy =  findViewById(R.id.arschloch);
        textView = (TextView)findViewById(R.id.machineId);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        _recy.setLayoutManager(manager);
        _recy.setHasFixedSize(true);
        setMachineId(savedInstanceState);
        MachineViewAdapter adapter = new MachineViewAdapter(newString);
        _recy.setAdapter(adapter);

    }



    public void setMachineId(Bundle savedInstanceState){

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                newString= null;
            } else {
                newString= extras.getString("MACHINE_ID");
            }
        } else {
            Bundle extras = getIntent().getExtras();
            newString= extras.getString("MACHINE_ID");
        }
        textView.setText(newString);
    }
}
