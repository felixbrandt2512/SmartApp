package com.example.alf.myapplication;


import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.alf.myapplication.MessagePOJO.Coordinates;
import com.example.alf.myapplication.MessagePOJO.MessageEvent;
import com.example.alf.myapplication.MessagePOJO.Player;
import com.example.alf.myapplication.MessagePOJO.PlayerEvent;
import com.example.alf.myapplication.persistance.MachineStorage;
import com.example.alf.myapplication.util.JsonParser;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationFragment extends Fragment {


    List<Player> players;
    HashMap<String,Coordinates> machines;

    public NotificationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_notification, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        //Create Players
        initMachines();
        initPlayers();
        RelativeLayout layout = getView().findViewById(R.id.playground);


        players = MachineStorage.newMachineStorage().getPlayers();





        for (Player p : players){
            ImageView player = createImageForPlayer();
            TextView text = new TextView(getContext());
            p.set_image(player);
            //Fuege alle Personen hinzu
            layout.addView(p.get_image());
        }


    }

    //Add all available Batches
    private void initPlayers() {
        List<Player> player = new ArrayList<Player>();
        Player p1 = new Player("Reichelt");player.add(p1);
        Player p2 = new Player("Felix");player.add(p2);
        Player p3 = new Player("Eric");player.add(p3);
        Player p5 = new Player("Florian");player.add(p5);
        MachineStorage.newMachineStorage().setPlayers(player);
    }

    private ImageView createImageForPlayer(){
        ImageView player = new ImageView(getView().getContext());
        player.setImageResource(R.mipmap.player_reich_round);
        player.setX(10);
        player.setY(10);
        return player;
    }

    public void initMachines(){

        DisplayMetrics metrics = this.getResources().getDisplayMetrics();
        int x = metrics.widthPixels;
        int y = metrics.heightPixels;
        machines = new HashMap<>();
        machines.put("CNC",new Coordinates(x*0.4f,y*0.7f));
        machines.put("Handarbeitsplatz",new Coordinates(x*0.8f,y*0.4f));
        machines.put("Roboter",new Coordinates(x*0.1f,y*0.1f));
        machines.put("Eingang",new Coordinates(x*0.8f,y*0.1f));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(PlayerEvent event)
    {

        Player  p = JsonParser.parsePlayer(event.position);
        String machine = JsonParser.getPlayerMachine(event.position);
        Coordinates coord = machines.get(machine);
        for (Player player : players){
            if(p.get_name().equals(player.get_name())){
                AnimatorSet set = new AnimatorSet();


                ObjectAnimator x = new ObjectAnimator().ofFloat(player.get_image(),"X",player.get_image().getX(),coord.getX());
                ObjectAnimator y = new ObjectAnimator().ofFloat(player.get_image(),"Y",player.get_image().getY(),coord.getY());
                player.get_image().setX(coord.getX());
                player.get_image().setY(coord.getY());
                set.playTogether(x,y);
                set.setInterpolator(new LinearInterpolator());
                set.setDuration(300);
                set.start();
                Log.d("PlayerEvent","PlayerEvent geworfen");
            }
        }

    }

    public void drawFactory(){

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }
}
