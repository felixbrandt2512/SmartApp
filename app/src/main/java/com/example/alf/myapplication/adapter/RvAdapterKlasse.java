package com.example.alf.myapplication.adapter;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.alf.myapplication.Notification;
import com.example.alf.myapplication.R;

import java.util.ArrayList;

public class RvAdapterKlasse extends RecyclerView.Adapter<RvAdapterKlasse.ViewHolderKlasse> {

    ArrayList<String> itemText1 = new ArrayList<>();

    public RvAdapterKlasse(ArrayList<String> liste){
        itemText1 = liste;
    }

    public class ViewHolderKlasse extends RecyclerView.ViewHolder
    {

        TextView itemTextView;
        ImageView itemImageView;


        public ViewHolderKlasse(View itemView){
            super(itemView);
            itemTextView = (TextView) itemView.findViewById(R.id.textViewItem);
            itemImageView = (ImageView) itemView.findViewById(R.id.imageViewItem);

        }
    }

    public void addToList(String string) {
        itemText1.add(string);
    }




    @NonNull
    @Override
    public ViewHolderKlasse onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        View itemView1 = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_layout1, null);
        return new ViewHolderKlasse(itemView1);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderKlasse holder, final int position) {



        holder.itemTextView.setText(itemText1.get(position));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Notification.tv1.setText(Notification.itemTexte.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return itemText1.size();
    }
}
