package com.example.alf.myapplication.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.alf.myapplication.MaschineView;
import com.example.alf.myapplication.R;
import com.example.alf.myapplication.persistance.MachineStorage;
import com.example.alf.myapplication.util.JsonParser;

public class MachineAdapter extends RecyclerView.Adapter<MachineAdapter.MachineViewHolder>{

    MachineStorage ms;
    String machineId;
    public MachineAdapter(){
        ms = MachineStorage.newMachineStorage();
    }




    @NonNull
    @Override
    public MachineAdapter.MachineViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutId = R.layout.machine_list_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttach = false;
        View view = inflater.inflate(layoutId,parent,false);
        MachineViewHolder vh = new MachineViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final MachineAdapter.MachineViewHolder holder, final int position) {

        final Context con = holder.itemView.getContext();
        machineId = ms.getSubscription().get(position).getMachine();
        holder.name.setText(machineId);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(con, MaschineView.class);
                Log.w("onBindViewHolder:","MS:id"+machineId);
                intent.putExtra("MACHINE_ID",ms.getSubscription().get(position).getMachine());
                con.startActivity(intent);
                Toast.makeText(con,"hello"+ms.getSubscription().get(position).getMachine(),Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        Log.w("getItemCount:","MS:size"+ms.getSubscription().size());
        return ms.getSubscription().size();
    }

    public class MachineViewHolder extends RecyclerView.ViewHolder {

        TextView name;

        public MachineViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.machineName);
        }

    }

}
