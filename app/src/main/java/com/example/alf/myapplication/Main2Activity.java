package com.example.alf.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.example.alf.myapplication.persistance.MachineStorage;
import com.example.alf.myapplication.services.AMQPPublisher;
import com.example.alf.myapplication.services.AMQPReceiver;

public class Main2Activity extends AppCompatActivity {

    private Intent receiverService;
    private Intent publishService;
    Fragment fragment;
    MachineStorage m;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            fragment = null;
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    fragment = new MachineFragment();
                    getSupportFragmentManager().beginTransaction().replace(R.id.container,fragment).commit();
                    //EventBus.getDefault().post(new MessageEvent("Hello everyone!"));
                    return true;
                case R.id.navigation_dashboard:
                    fragment = new NotificationFragment();
                    getSupportFragmentManager().beginTransaction().replace(R.id.container,fragment).commit();
                    return true;
                case R.id.navigation_notifications:
                    fragment = new Notification();
                    getSupportFragmentManager().beginTransaction().replace(R.id.container,fragment).commit();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        //StartAMQP ReceiveService
        receiverService = new Intent(this, AMQPReceiver.class);
        startService(receiverService);

        //StartAMQP PublishService
        publishService = new Intent(this, AMQPPublisher.class);
        startService(publishService);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        fragment = new MachineFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.container,fragment).commit();
    }

}
