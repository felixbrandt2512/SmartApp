package com.example.alf.myapplication.persistance;

import com.example.alf.myapplication.MessagePOJO.MessageEvent;
import com.example.alf.myapplication.MessagePOJO.Player;
import com.example.alf.myapplication.MessagePOJO.Subscription;
import com.example.alf.myapplication.util.JsonParser;

import java.util.ArrayList;
import java.util.List;

public class MachineStorage {

    private static MachineStorage instance;

    private List<Subscription> machines;
    private List<MessageEvent> messages;
    private List<Player> players;

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public List<MessageEvent> getMessages() {
        return messages;
    }

    public void addMessage(MessageEvent mess) {
        this.messages.add(mess);
    }

    private MachineStorage(){

                machines = new ArrayList<>();
                messages = new ArrayList<>();
                players = new ArrayList<>();
    }

    public static MachineStorage newMachineStorage(){
        if(instance == null)
            instance = new MachineStorage();
        return instance;
    }

    public List<Subscription> getSubscription(){
        return machines;
    }

    public boolean addSubscription(Subscription m){
        boolean check = true;
        for(Subscription s : machines){
            for (String topic : s.getTopics()){
                if(topic.equals(m.getMachine())) {
                    check = false;
                }
            }
        }
        if(check)machines.add(m);
        return check;
    }

    public void addPlayer(Player p){
        boolean check = true;
        for(Player s : players){
                if(s.get_name().equals(p.get_name())) {
                    check = false;
                }
        }
        if(check)players.add(p);
    }

    public Subscription getSubscriptionById(String machineId){
        Subscription sub = new Subscription("tmp");
        for(Subscription s : machines){
            if(s.getMachine().equals(machineId)) sub = s;
        }
        return sub;
    }

}
