package com.example.alf.myapplication;


import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.alf.myapplication.MessagePOJO.Subscription;
import com.example.alf.myapplication.adapter.MachineAdapter;
import com.example.alf.myapplication.persistance.MachineStorage;
import com.example.alf.myapplication.util.JsonParser;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;

import org.greenrobot.eventbus.EventBus;


/**
 * A simple {@link Fragment} subclass.
 */
public class MachineFragment extends Fragment {

    private MachineAdapter adapter;
    private RecyclerView _recy;
    private FloatingActionButton button;
    private static final int RC_BARCODE_CAPTURE = 9001;
    MachineStorage ms;
    public MachineFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_machine, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        _recy = (RecyclerView) getView().findViewById(R.id.rv_machines2);
        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        _recy.setLayoutManager(manager);
        _recy.setHasFixedSize(true);

        button =  (FloatingActionButton) getView().findViewById(R.id.addMachine);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), QRCapture.class);

                //check for Internet
                if(isNetworkConnected()){
                    startActivityForResult(intent, RC_BARCODE_CAPTURE);
                }else{
                    Toast.makeText(getContext(),"keine InternetVerbindung",Toast.LENGTH_SHORT).show();
                }

            }
        });
        //onlickhandler


        adapter = new MachineAdapter();

        _recy.setAdapter(adapter);
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_BARCODE_CAPTURE) {
            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    Barcode barcode = data.getParcelableExtra(QRCapture.BarcodeObject);
                    Log.d("onActivityResult", "barcode captured: "+ barcode.rawValue);

                    Subscription m = JsonParser.parseMessage(barcode.rawValue);
                    ms = MachineStorage.newMachineStorage();
                    ms.addSubscription(m);
                    EventBus.getDefault().post(m.getMachine());

                    adapter.notifyItemInserted(adapter.getItemCount()-1);
                } else{
                    Log.d("Debug", "No barcode captured, intent data is null");
                }
            } else {
                //statusMessage.setText(String.format(getString(R.string.barcode_error),
                //CommonStatusCodes.getStatusCodeString(resultCode)));
                Log.d("Shit", "Esle");
            }
        }
        else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }



}
