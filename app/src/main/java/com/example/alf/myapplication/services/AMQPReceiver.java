package com.example.alf.myapplication.services;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.EventLog;
import android.util.Log;
import android.widget.Toast;

import com.example.alf.myapplication.MessagePOJO.MessageEvent;
import com.example.alf.myapplication.MessagePOJO.PlayerEvent;
import com.example.alf.myapplication.MessagePOJO.Subscription;
import com.example.alf.myapplication.persistance.MachineStorage;
import com.example.alf.myapplication.util.JsonParser;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

public class AMQPReceiver extends Service {

    private EventBus eventBus;
    private Handler handler;

    private Thread subscribeT;
    private ConnectionFactory factory;

    Connection connection;
    Channel channel;
    AMQP.Queue.DeclareOk q;

    private List<String> tempTopics;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        eventBus = EventBus.getDefault();
        tempTopics = new ArrayList<>();

        handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                String message = msg.getData().getString("msg");
                MachineStorage m = MachineStorage.newMachineStorage();
                if(message.contains("move")){
                    m.addPlayer(JsonParser.parsePlayer(message));
                    eventBus.post(new PlayerEvent(message));
                }else {
                    Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
                    m.addMessage(new MessageEvent(message));
                    sendNachricht(message);
                }
            }
        };

    }

    @Subscribe
    public void sendNachricht(String message){
        eventBus.post(new MessageEvent(message));
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onMessage(MessageEvent event){
        subscribe(event);
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(!eventBus.isRegistered(this)){
            eventBus.register(this);
        }

        if (factory == null){
            setupConnection();
            setupSubscription();
        }
        return START_STICKY;
        
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("AMQPReceiver", "onDestroy called");
        eventBus.unregister(this);
        subscribeT.interrupt();
    }

    //Nach Event neue Elemente Registrieren
    //Nur wenn Element nicht schon Vorhanden ist

    private void subscribe(MessageEvent msg ) {
        Log.d("EventDebug",msg.message);
        MachineStorage ms = MachineStorage.newMachineStorage();

        for (Subscription s : ms.getSubscription()){
            for (String topic : s.getTopics()){
                    try {
                        channel.queueBind(q.getQueue(), "amq.topic",topic);
                        Log.d("QueueBind: ",topic);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
            }
        }
    }

    private void setupSubscription() {
        if (subscribeT != null) {
            subscribeT.interrupt();
            subscribeT.start();
            return;
        }

        subscribeT = new Thread(new Runnable(){

            @Override
            public void run() {
                boolean connStatus =true;
                while(true) {
                    try {
                        connection= factory.newConnection();
                        channel = connection.createChannel();

                        channel.basicQos(1);
                        q = channel.queueDeclare();
                        QueueingConsumer consumer = new QueueingConsumer(channel);
                        channel.basicConsume(q.getQueue(), true, consumer);
                        if(connection.isOpen()){
                            Log.d("Reconnect","Subscribe neu");
                            subscribe(new MessageEvent("con"));
                            connStatus = true;
                        }
                        while (true) {
                            QueueingConsumer.Delivery delivery = consumer.nextDelivery();
                            String message = new String(delivery.getBody());
                            Log.d("","[r] " + message);
                            Message msg = handler.obtainMessage();
                            Bundle bundle = new Bundle();
                            bundle.putString("msg", message);
                            msg.setData(bundle);
                            handler.sendMessage(msg);
                        }
                    } catch (InterruptedException e) {
                        break;
                    } catch (Exception e1) {
                        connStatus = false;
                        Log.d("", "Connection broken: " + e1.toString());
                        try {
                            Thread.sleep(5000); //sleep and then try again
                        } catch (InterruptedException e) {
                            break;
                        }
                    }
                }
            }
        });
        subscribeT.start();
    }


    private void setupConnection() {
        String uri = "amqp://username:password@golden-kangaroo.rmq.cloudamqp.com/username";
        String userNAme = "your username";
        String password = "your password";
        factory = new ConnectionFactory();
        try {
            factory.setAutomaticRecoveryEnabled(false);
            factory.setUri(uri);
            factory.setUsername(userNAme);
            factory.setPassword(password);
        } catch (KeyManagementException | NoSuchAlgorithmException | URISyntaxException e1) {
            e1.printStackTrace();
        }
    }
}
