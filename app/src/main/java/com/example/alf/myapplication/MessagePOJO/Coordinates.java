package com.example.alf.myapplication.MessagePOJO;

public class Coordinates {
    private Float X;
    private Float Y;

    public Coordinates(Float x, Float y) {
        X = x;
        Y = y;
    }

    public Float getX() {
        return X;
    }

    public void setX(Float x) {
        X = x;
    }

    public Float getY() {
        return Y;
    }

    public void setY(Float y) {
        Y = y;
    }
}
