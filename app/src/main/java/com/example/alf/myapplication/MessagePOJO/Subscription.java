package com.example.alf.myapplication.MessagePOJO;

import java.util.List;

public class Subscription {
    String machine;
    List<String> topics;
    List<String> events;
    String subscription;

    public Subscription(String sub){
        subscription = sub;
    }

    public String getSubscription() {
        return subscription;
    }

    public void setSubscription(String subscription) {
        this.subscription = subscription;
    }

    public Subscription(String machine, List<String> topics, List<String> events) {
        this.machine = machine;
        this.topics = topics;
        this.events = events;
    }

    public String getMachine() {
        return machine;
    }

    public void setMachine(String machine) {
        this.machine = machine;
    }

    public List<String> getTopics() {
        return topics;
    }

    public void setTopics(List<String> topics) {
        this.topics = topics;
    }

    public List<String> getEvents() {
        return events;
    }

    public void setEvents(List<String> events) {
        this.events = events;
    }
}
