package com.example.alf.myapplication;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.alf.myapplication.MessagePOJO.MessageEvent;
import com.example.alf.myapplication.adapter.RvAdapterKlasse;
import com.example.alf.myapplication.persistance.MachineStorage;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Arrays;


/**
 * A simple {@link Fragment} subclass.
 */
public class Notification extends Fragment {

    RecyclerView recyclerView1;
//    RecyclerView.Adapter rvadapter1;
    RvAdapterKlasse rvadapter1;
    RecyclerView.LayoutManager rvLayoutManager1;

    public static ArrayList<String> itemTexte;
    public static ArrayList<Integer> itemFotoIDs;

    public MachineStorage ms;


    public Notification() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment




        return inflater.inflate(R.layout.fragment_notification2, container, false);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(MessageEvent event){

        Log.w("String", rvadapter1.getItemCount() + "LOL");
        rvadapter1.addToList(event.message);
        rvadapter1.notifyDataSetChanged();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        itemTexte = new ArrayList<>();
        itemFotoIDs = new ArrayList<>();

        ms = MachineStorage.newMachineStorage();

        itemFotoIDs.addAll(Arrays.asList(R.drawable.common_ic_googleplayservices));

        recyclerView1 = (RecyclerView) getView().findViewById(R.id.my_recycler_view);
        rvLayoutManager1 = new LinearLayoutManager(getContext());
        recyclerView1.setLayoutManager(rvLayoutManager1);

        ArrayList<String> liste = new ArrayList<>();

        rvadapter1 = new RvAdapterKlasse(liste);
        recyclerView1.setAdapter(rvadapter1);


        if(ms.getMessages().size() > 0){
            for (MessageEvent m:ms.getMessages()){
                rvadapter1.addToList(m.message);
                rvadapter1.notifyDataSetChanged();
            }
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }
}
