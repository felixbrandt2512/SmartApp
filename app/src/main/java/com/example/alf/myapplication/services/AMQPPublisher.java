package com.example.alf.myapplication.services;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.example.alf.myapplication.MessagePOJO.MessageEvent;
import com.example.alf.myapplication.MessagePOJO.PublishEvent;
import com.example.alf.myapplication.persistance.MachineStorage;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

public class AMQPPublisher extends Service {

    private Thread publishT;
    private ConnectionFactory factory;
    private EventBus eventBus;
    Connection connection;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private static BlockingDeque<String> queue = new LinkedBlockingDeque<String>();

    @Subscribe()
    public void publishMessage(PublishEvent message) {
        try {
            Log.d("publishMessage","[q] " + message.publishMessage);
            queue.putLast(message.publishMessage);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate() {
        eventBus = EventBus.getDefault();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(!eventBus.isRegistered(this)){
            eventBus.register(this);
        }

        if (factory == null){
            setupConnection();
            publishToAMQP();
        }
        return START_STICKY;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("AMQPReceiver", "onDestroy called");
        eventBus.unregister(this);
        publishT.interrupt();
    }

    public void publishToAMQP()
    {
        publishT = new Thread(new Runnable() {
            @Override
            public void run() {
                while(true) {
                    try {
                        Connection connection = factory.newConnection();
                        Channel ch = connection.createChannel();
                        ch.confirmSelect();

                        while (true) {
                            String message = queue.takeFirst();
                            try{
                                ch.basicPublish("amq.topic", message, null, message.getBytes());
                                Log.d("arsch", "[s] " + message);
                                ch.waitForConfirmsOrDie();
                            } catch (Exception e){
                                Log.d("","[f] " + message);
                                queue.putFirst(message);
                                throw e;
                            }
                        }
                    } catch (InterruptedException e) {
                        break;
                    } catch (Exception e) {
                        Log.d("", "Connection broken: " + e.getClass().getName());
                        try {
                            Thread.sleep(5000); //sleep and then try again
                        } catch (InterruptedException e1) {
                            break;
                        }
                    }
                }
            }
        });
        publishT.start();
    }


    private void setupConnection() {
       String uri = "amqp://username:password@golden-kangaroo.rmq.cloudamqp.com/username";
        String userNAme = "your username";
        String password = "your password";
        factory = new ConnectionFactory();
        try {
            factory.setAutomaticRecoveryEnabled(false);
            factory.setUri(uri);
            factory.setUsername(userNAme);
            factory.setPassword(password);
        } catch (KeyManagementException | NoSuchAlgorithmException | URISyntaxException e1) {
            e1.printStackTrace();
        }
    }


}
