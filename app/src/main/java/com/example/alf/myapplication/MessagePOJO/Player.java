package com.example.alf.myapplication.MessagePOJO;

import android.widget.ImageView;
import android.widget.TextView;

public class Player {
    private String _name;
    private int X;
    private int Y;
    private ImageView _image;


    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public ImageView get_image() {
        return _image;
    }

    public void set_image(ImageView _image) {
        this._image = _image;
    }

    public Player(String _name) {
        this._name = _name;
        this.X = 0;
        this.Y = 0;
    }
}
