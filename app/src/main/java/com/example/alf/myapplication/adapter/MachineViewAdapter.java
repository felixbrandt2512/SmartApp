package com.example.alf.myapplication.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.alf.myapplication.MessagePOJO.PublishEvent;
import com.example.alf.myapplication.R;
import com.example.alf.myapplication.persistance.MachineStorage;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

public class MachineViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<String> events;
    List<String> topics;
    int eventssize = 0;
    int topicsize = 0;
    String e;

    public MachineViewAdapter(String machineId){
        MachineStorage ms = MachineStorage.newMachineStorage();

        events = ms.getSubscriptionById(machineId).getEvents();
        topics = ms.getSubscriptionById(machineId).getTopics();
        eventssize = events.size();
        topicsize = topics.size();
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        Context context = parent.getContext();
        int layoutId = R.layout.machine_list_item;
        int eventId = R.layout.machine_event_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttach = false;
        View view = inflater.inflate(layoutId,parent,false);
        View eventView = inflater.inflate(eventId,parent,false);
        Log.w("TYPE",viewType+"");
        switch(viewType){
            case 1: return new EventViewHolder(eventView);
            case 0: return new TopicViewHolder(view);
        }


        MachineViewAdapter.ViewHolder vh = new MachineViewAdapter.ViewHolder(view);
        return vh;
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder,final int position) {
        final Context con = holder.itemView.getContext();

        if(position < eventssize){
            EventViewHolder viewHolder0 = (EventViewHolder)holder;
            viewHolder0.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.w("eventListener:",events.get(position));
                    EventBus.getDefault().post(new PublishEvent(events.get(position)));
                }
            });
            viewHolder0.name.setText(events.get(position));
        }else {
            TopicViewHolder viewHolder2 = (TopicViewHolder)holder;
            viewHolder2.name.setText(topics.get(position-eventssize));
        }
    }

    @Override
    public int getItemViewType(int position) {
        // Just as an example, return 0 or 2 depending on position
        // Note that unlike in ListView adapters, types don't have to be contiguous


        if(position < eventssize)
            return 1;
        else return 0;
    }

    @Override
    public int getItemCount() {
        return eventssize+topicsize;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name;

        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.machineName);
        }

    }

    public class EventViewHolder extends RecyclerView.ViewHolder {

        TextView name;

        public EventViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.eventName);

        }

    }

    public class TopicViewHolder extends RecyclerView.ViewHolder {

        TextView name;

        public TopicViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.machineName);
        }

    }
}
