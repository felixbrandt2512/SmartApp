# Welcome to SmartFactory App

In diesem kleinen Projekt soll eine Android App entstehen, Maschinendaten der SmartFactory HTW Dresden anzeigt. Über geeignete Schnittstellen (QR/NFC) sollen Topics an die App übergeben werden, welche daraufhin diese an einem MessageBroker registriert.

## MessageBroker
Wir werden in unserem Projekt den RabbitMQ Broker verwenden. Dieser bietet mehrere Nachrichtenprotokolle an, wobei wir uns auf das AMQP Protokoll geeinigt haben. 

## Anforderungen an die App

|Anforderung|  |
|--|--|
|QR scan  | Topic übermitteln, diese subscriben und an eine Liste innerhalb der App anzeigen |
|LED  | LED event ansteuern |
|RTLS  | Positionen von Personen anzeigen die sich im Raum bewegen (optional) |
|Maschine steuern  | einfaches Event zur Manipulation der Maschine |



##  Design der App
  
![bild](https://gitlab.com/felixbrandt2512/SmartFactoryApp/raw/master/rtls.png)


## Roadmap

-> ~~Grundlegende Architektur der App festlegen -> erster Prototyp(Design studie)~~  

-> ~~Bibliotheken für RabbitMQ/QRScan prüfen -> einbinden und erste Daten übermitteln~~  

-> ~~Subscription -> App aktionen ausführen lassen auf basis der Subscription~~  

-> ~~Architektur review/ polishing~~  

-> ~~fortgeschrittene Anforderungen implementieren~~  

## Interessante Links  
sending data from service to fragment  
https://stackoverflow.com/questions/41375154/web-socket-sending-and-receiving-data-in-multiple-fragments-and-activity  
https://github.com/eclipse/paho.mqtt.android/tree/master/paho.mqtt.android.example/src/main/java/paho/mqtt/java/example
https://github.com/greenrobot/EventBus


## RabbitMQ Message


```
{ 
  "machine":"Handarbeitsplatz",  
  "topics":["Handarbeitsplatz.ledevent","Handarbeitsplatz.energie"],
  "events":["ledOn","Handarbeitsplatz.flagOn"]
}
```
![bild](https://gitlab.com/felixbrandt2512/SmartFactoryApp/raw/master/Handarbeitsplatz_qr.PNG)

## IBM Bluemix
https://console.bluemix.net/


